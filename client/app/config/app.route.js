(function () {
    angular
        .module("GMS")
        .config(uiRouteConfig);

    uiRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uiRouteConfig($stateProvider, $urlRouterProvider){
        $stateProvider            
            .state('searchDB',{
                url : '/searchDB',
                templateUrl: "app/search/searchDB.html",
                controller: 'SearchDBCtrl',
                controllerAs: "ctrl"
            })
            .state('edit',{
                url : '/edit',
                templateUrl: "app/edit/edit.html",
                controller: 'EditCtrl',
                controllerAs: "ctrl"
            })
            .state('editWithParams',{
                url : '/edit/:id',
                templateUrl: "app/edit/edit.html",
                controller: 'EditCtrl',
                controllerAs: "ctrl"           
            });
        $urlRouterProvider.otherwise("/searchDB");
    
    }

})();