(function () {
    angular
        .module("GMS")
        .controller("SearchDBCtrl", SearchDBCtrl);

    SearchDBCtrl.$inject = ['GroceryService','$state'];

    function SearchDBCtrl(GroceryService, $state) {
        var vm = this;

        vm.id = "";
        vm.upc12 = null;
        vm.name ="";
        vm.brand = "";
        vm.searchStringName = '';
        vm.searchStringBrand = '';
        vm.result = null;
      

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        //vm.searchDB = searchDB; //Default
        vm.searchByName = searchByName; //ByProduct Name
        vm.searchByBrand = searchByBrand; //By Brand
        vm.goToEdit = goToEdit; //Edit Product name, brand, bar code

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)
        init();

        // Function declaration and definition -------------------------------------------------------------------------
        // The init function initializes view
        function init() {
            GroceryService
                .retrieveGroceryDB('')
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.groceries = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }

        // The search function searches for departments that matches query string entered by user. The query string is
        // matched against the department name and department number alike.
        function searchByName() {
            //console.log(vm.searchStringName);  
            GroceryService
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrieveGroceryByName(vm.searchStringName)
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    console.log("results: " + JSON.stringify(results.data));
                    vm.groceries = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }

        function searchByBrand() {        
            //console.log(vm.searchStringBrand);
            GroceryService
            // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrieveGroceryByBrand(vm.searchStringBrand)
                .then(function (results){
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    console.log("results: " + JSON.stringify(results.data));
                    vm.groceries = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.info("error " + JSON.stringify(err));
                });
        }

        function goToEdit(id){
            $state.go("editWithParams",{id:id});
        }
    }
})();