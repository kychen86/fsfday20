// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches DeptService service to the DMS module
    angular
        .module("GMS")
        .service("GroceryService", GroceryService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    GroceryService.$inject = ['$http'];

    // DeptService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function GroceryService($http) {

        // Declares the var service and assigns it the object this (in this case, the DeptService). Any function or
        // variable that you attach to service will be exposed to callers of DeptService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        
        service.retrieveGroceryById = retrieveGroceryById;
        service.retrieveGroceryDB = retrieveGroceryDB;
        service.retrieveGroceryByBrand = retrieveGroceryByBrand;
        service.retrieveGroceryByName = retrieveGroceryByName;
        service.updateGrocery = updateGrocery;


        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------

        // deleteDept uses HTTP DELETE to delete department from database; passes information as route parameters.
        // IMPORTANT! Route parameters are not the same as query strings!
        
        // retrieveDeptDB retrieves department information from the server via HTTP GET. Passes information via the query
        // string (params) Parameters: searchString. Returns: Promise object
        function retrieveGroceryDB() {
            return $http({
                method: 'GET',
                url: 'api/groceriesDB',
               
            }); //find default
        }

        // retrieveDeptByID retrieves department information from the server via HTTP GET. Passes information as a
        function retrieveGroceryById(id) {
            return $http({
                method: 'GET',
                url: "api/groceries/" + id
            });
        }
        
        // route parameter
        function retrieveGroceryByBrand(searchString) {
            //console.log("Retrieve BrandClient");
            return $http({
                method: 'GET',
                url: 'api/groceries/brand',
                params: {
                    'searchString': searchString
                }
            });
        }

        // retrieveDeptManager retrieves department information from the server via HTTP GET.
        // Parameters: searchString. Returns: Promise object
        function retrieveGroceryByName(searchString) {
            return $http({
                method: 'GET',
                url: 'api/groceries/name',
                params: {
                    'searchString': searchString
                }
            });
        }

        // updateDept uses HTTP PUT to update department name saved in DB; passes information as route parameters and via
        // HTTP HEADER BODY IMPORTANT! Route parameters are not the same as query strings!
        function updateGrocery(id,brand,name,upc12) {
            return $http({
                method: 'PUT'
                , url: 'api/groceries/' + id
                , data: {
                    id: id,
                    brand: brand,
                    name: name,
                    upc12: upc12
                }
            });
        }
    }
})();