// Read configurations
var config = require('./config');

// Load Sequelize package
var Sequelize = require("sequelize");

// Create Sequelize DB connection
var sequelize = new Sequelize( // input parameters, database,username,password, and options
	'grocery',
	config.MYSQL_USERNAME,
	config.MYSQL_PASSWORD,
	{
		host: config.MYSQL_HOSTNAME,
		port: config.MYSQL_PORT,
		logging: config.MYSQL_LOGGING,
		dialect: 'mysql',
		pool: {	//can be set in config
			max: 5,
			min: 0,
			idle: 10000,
		},
	}
);

// Import DB Models
const Grocery = sequelize.import('./models/grocery');

// Define Model Associations
// Exports Models
module.exports = { //setting up the models attached to db.js to be used by others
  // Loads model for departments table
  Grocery: Grocery,
};
