// Handles department DB queries

// Retrieve from DB
var retrieveDB = function(db) {
  return function (req, res) {
    
    db.Grocery //db.Grocery refer to db.js, Grocery model that is why you need to import, export model at db.js
      .findAll({
        where: {
            
        },
          order: [["name", "ASC"]], 
          limit: 20,
      })
      .then(function(groceries) {
        res
          .status(200)
          .json(groceries);
      })
      .catch(function(err) {
        console.log("groceriesDB error clause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};
//Search by brand
var retrieveByBrand = function(db) {
  return function (req, res) {
    //console.log("Retrieve Brand 1");
    if(!req.query.searchString){req.query.searchString = ''}
    //console.log("Retrieve Brand");
    db.Grocery 
      .findAll({
        where: {          
          brand: { $like: "%" + req.query.searchString + "%" }
        },
        order: [["name", "ASC"]], 
        limit: 20,
      })
      .then(function(groceries) {
        console.log(groceries);
        res
          .status(200)
          .json(groceries);
      })
      .catch(function(err) {
        console.log("Brand search error clause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};
//Search by product name
var retrieveByName = function(db) {
  return function (req, res) {
    if(!req.query.searchString){req.query.searchString = ''}
    db.Grocery 
      .findAll({
        where: {          
          name: { $like: "%" + req.query.searchString + "%" }
        },
        order: [["name", "ASC"]], 
        limit: 20,        
      })
      .then(function(groceries) {
        res
          .status(200)
          .json(groceries);
      })
      .catch(function(err) {
        console.log("Name search error clause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};
//retrieveBy ID
var retrieveById = function(db){
  return function (req, res) {
      console.log
      var where = {};
      if (req.params.id) {
          where.id = parseInt(req.params.id)
      }
      console.log("where " + where);
      db.Grocery
          .findOne({
              where: where
          })
          .then(function (groceries) {
              console.log("-- GET /api/groceries/:id findOne then() result \n " + JSON.stringify(groceries));
              res.json(groceries);
          })
          // this .catch() handles erroneous findAll operation
          .catch(function (err) {
              console.log("-- GET /api/groceries/:id findOne catch() \n " + JSON.stringify(groceries));
              res
                  .status(500)
                  .json({error: true});
          });

  }
};

// -- Updates department info
var updateById = function(db){
  return function (req, res) {
      var where = {};
      // *** Updates department detail
      db.Grocery
          .update({            
                      upc12: parseInt(req.body.upc12),
                      brand: req.body.brand,
                      name : req.body.name,                      
                  },
                  {
                      where: {
                        id : parseInt(req.body.id) // could also use where as it is defined on line 287
                      }
                  }
          )
          .then(function(result){
              console.log(result);
              res
                  .status(200)
                  .json({success: result});
          })
          .catch(function(err){
              console.log("Error");
              console.log(err);
              res
                  .status(500)
                  .json({success:false});
          })
    }      
  };


// Export route handlers
module.exports = function(db) {
  return {    
    retrieveDB: retrieveDB(db),
    updateById: updateById(db),
    retrieveByBrand: retrieveByBrand(db),
    retrieveByName: retrieveByName(db),
    retrieveById: retrieveById(db),
  }
};
