
//Model for grocery_list table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
module.exports = function (sequelize, Sequelize) {
    var Grocery = sequelize.define("grocery",
        {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                allowNull: false
            },
            upc12: {
                type: Sequelize.BIGINT(11),
                allowNull: false
            },
            brand: {
                type: Sequelize.STRING,
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false,
            tableName: "grocery_list"
         });

    return Grocery;
};