// Handles API routes

module.exports = function(app, db) {
  var Grocery = require('./api/grocery.controller')(db); //the app and db here are place holders, values are to be decided at app.js

 
  // Retrieve all grocery records that match query string passed. 
  
  app.get("/api/groceriesDB", Grocery.retrieveDB);
  app.get("/api/groceries/brand", Grocery.retrieveByBrand);
  app.get("/api/groceries/name", Grocery.retrieveByName);
  app.put("/api/groceries/:id", Grocery.updateById);
  app.get("/api/groceries/:id", Grocery.retrieveById);
};
